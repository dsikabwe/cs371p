% which docker
/usr/local/bin/docker



% docker --version
Docker version 18.09.1, build 4c52b90



% docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE



% cat Dockerfile
FROM openjdk

RUN apt-get update          && \
    apt-get -y install vim

CMD bash



% docker build -t gpdowning/java .
...



% docker login
...



% docker push gpdowning/java
...



% docker pull gpdowning/java
...



% docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
gpdowning/java      latest              95cd9f4b699e        7 days ago          1.03GB
openjdk             latest              22b2c9245dfe        6 weeks ago         976MB



% pwd
/Users/downing/cs373/git


% ls
README	_CS373.bbprojectd  examples  makefile  notes  projects



% docker run -it -v /Users/downing/cs373/git:/usr/java -w /usr/java gpdowning/java

root@9a51508a09c9:/usr/java# pwd
/usr/java



root@9a51508a09c9:/usr/java# ls
README	_CS373.bbprojectd  examples  makefile  notes  projects



root@9a51508a09c9:/usr/java# which git
/usr/bin/git
root@9a51508a09c9:/usr/java# git --version
git version 2.19.1



root@9a51508a09c9:/usr/java# which java
/usr/bin/java
root@9a51508a09c9:/usr/java# java --version
openjdk 11.0.1 2018-10-16



root@9a51508a09c9:/usr/java# which javac
/usr/bin/javac
root@9a51508a09c9:/usr/java# javac --version
javac 11.0.1



root@9a51508a09c9:/usr/java# which make
/usr/bin/make
root@9a51508a09c9:/usr/java# make --version
GNU Make 4.2.1



root@9a51508a09c9:/usr/java# which vim
/usr/bin/vim
root@9a51508a09c9:/usr/java# vim --version
VIM - Vi IMproved 8.1 (2018 May 18, compiled Nov 28 2018 01:38:28)
