// -----------
// Fri,  8 Feb
// -----------

/*
FoCS
    please track and go
    really worth your time

Lab Sessions
    Wed, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF, 12-12:45pm,   GDC 6.302
        T,  3:30-4:10pm,  Zoom
    Brian
        M,  12-1pm,       GDC 3.302
    Katherine
        T,  9:30-10:30am, GDC 1.302

Piazza
    ask and answer questions
    please be proactive
*/

int i = 2;
cout << i;  // 2
cout << &i; // <address of i>

int  i = 2;
int* p;     // does not have to be initialized
p = &i;

int  i = 2;
int& r;     // no, does have to be initialized

void f (int v) { // by value
    ++v;}

int main () {
    int i = 2;
    f(i);
    cout << i;   // 2
    return 0;}

void f (int* p) { // by address
    ++p;          // undefined
    ++*p;}

int main () {
    int i = 2;
    f(i);        // no
    f(&i);
    cout << i;   // 3
    return 0;}

void f (int& r) { // by reference
    ++*r;         // no
    ++r;}

int main () {
    int i = 2;
    f(i);
    f(&i);       // no
    cout << i;   // 3
    return 0;}

void f (int* p) {
    // <check that p is not null>
    ++*p;}

int main () {
    int i = 2;
    f(&i);
    f(185);    // no
    f(0);      // maybe undefined
    return 0;}

/*
a good OO language, should turn some runtime errors into compile time errors
*/

void f (int& r) { // requires an l-value
    ++r;}

int main () {
    int i = 2;
    f(i);
    f(185);    // no, not an l-value
    f(0);      // no, not an l-value
    return 0;}

int& pre_incr (int& r) {
    return r += 1;}

int post_incr (int& r) {
    int v = r;
    r += 1;
    return v;}
