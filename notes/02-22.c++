// -----------
// Fri, 22 Feb
// -----------

/*
FoCS
    please track and go
    really worth your time

Lab Sessions
    Wed, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF,  12-12:45pm,   GDC 6.302
        TTh, 3-4pm,        Zoom, https://us04web.zoom.us/j/412178924
    Brian
        M,   12-1pm,       GDC 3.302
    Katherine
        T,   9:30-10:30am, GDC 1.302

Piazza
    ask and answer questions
    please be proactive
*/

/*
Java

interface List {
    ? get (int i);
    ...}

List is implemented by :
    ArrayList  (array)
    ArrayDeque (array)
    LinkedList (doubly-linked list)

definition of sort() on List is very slow when LinkedList is used
*/

/*
C++
definition of sort() of list<T> doesn't compile
*/

/*
accumulate takes four arguments
    begin iterator
    end   iterator
    starting value
    binary function
*/

/*
v # a0 # a1 # a2 # ... # an-1
*/

/*
seq of numbers
0
addition
sum
*/

/*
seq of numbers
1
multiplication
product
*/

/*
1..n
1
multiplication
factorial
*/

struct A {
    ? operator () (...) {
        ...}};

A* p = new A(); // this does not invoke the () operator
                // default constructor

A x; // default constructor

? y = x.operator()(...);
? y = x(...)

template <typename II, typename T, typename BF>
... accumulate (II b, II e, T v, BF f) {
    ...
    f(...)
    ...

template <typename T>
T my_multiplies (const T& x, const T& y) {
    return x * y;}

template <typename T>
T my_plus (const T& x, const T& y) {
    return x + y;}

T x = accumulate(b, e, v, my_multiplies<T>); // BF -> function<T (T, T)>
T x = accumulate(b, e, v, my_plus<T>);       // BF -> function<T (T, T)>

template <typename T>
struct my_multiplies {
    T operator () (const T& x, const T& y) const {
        return x * y;}};

template <typename T>
struct my_plus {
    T operator () (const T& x, const T& y) const {
        return x + y;}};

T x = accumulate(b, e, v, my_multiplies<T>()); // BF -> my_multiplies<T>
T x = accumulate(b, e, v, my_plus<T>());       // BF -> my_plus<T>
