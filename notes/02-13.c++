// -----------
// Wed, 13 Feb
// -----------

/*
FoCS
    please track and go
    really worth your time

Lab Sessions
    Wed, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF,  12-12:45pm,   GDC 6.302
        TTh, 3-4pm,        Zoom, https://us04web.zoom.us/j/412178924
    Brian
        M,   12-1pm,       GDC 3.302
    Katherine
        T,   9:30-10:30am, GDC 1.302

Piazza
    ask and answer questions
    please be proactive
*/

// let T be a user-defined type

const int s = 10;
T* a[s] = {2, 3, 4};

T(int) // 3 times
T()    // 7 times

// Java

class A {}

// what do you get for free in Java

A()        // default constructor
toString()
equals()

// C++

class A {};

// what do you get for free in C++

A()                 // default constructor
A(const A&)         // copy    constructor
operator=(const A&) // copy    assignment
~A()                // destructor

const int s = 10;
int a[s];         // uninitialized, O(1)
T   a[s];         // T(), s times,  O(n)

int a[s] = {}; // 0,   O(n)
T   a[s] = {}; // T(), O(n)

bool b = equal(a + 5, a + 15, b + 25);

/*
how many elements, at least, would a have to have?
a needs at least 15 elements

how many elements, at least, would b have to have?
b needs at least 35 elements
*/

int i = 2;
int j = i; // make a copy of int i

T x = ...;
T y = x;   // make a copy of T x

T a[] = {...};
T b[] = a;     // no, arrays can not be copied
