// -----------
// Wed,  6 Mar
// -----------

/*
FoCS
    please track and go
    really worth your time

Lab Sessions
    Wed, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF,  12-12:45pm,   GDC 6.302
        TTh, 3-4pm,        Zoom, https://us04web.zoom.us/j/412178924
    Brian
        M,   12-1pm,       GDC 3.302
    Katherine
        T,   9:30-10:30am, GDC 1.302

Piazza
    ask and answer questions
    please be proactive
*/

RangeIterator<int> b(2);

class Mammal {...};
class Tiger  : public Mammal {...};

int main () {
    Tiger*  p = new Mammal(...); // no
    Mammal* p = new Tiger(...);
    return 0;}

/*
vector
pop_back
push_back
does NOT have pop_front
does NOT have push_front
implemented with an array (front-loaded)
cost of adding to the front : O(n)
cost of adding to the middle : O(n)
cost of adding to the back : amortized O(1)
cost of removing from the front : O(n)
cost of removing from the middle : O(n)
cost of removing from the back: O(1)
cost of indexing : O(1)
cost of resizing : O(n)
*/

/*
deque
pop_back
push_back
pop_front
push_front
implemented with an an array of arrays
cost of adding to the front : amortized O(1)
cost of adding to the middle : O(n)
cost of adding to the back : amortized O(1)
cost of removing from the front : O(1)
cost of removing from the middle : O(n)
cost of removing from the back: O(1)
cost of indexing : O(1), much more expensive than vector's
cost of resizing : O(n), much less expensive than vector's
*/

/*
list
pop_back
push_back
pop_front
push_front
implemented with an an doubly-linked list
cost of adding to the front : O(1), more expensive than vector's or deque's
cost of adding to the middle : O(1)
cost of adding to the back : O(1), more expensive than vector's or deque's
cost of removing from the front : O(1), more expensive than deque's
cost of removing from the middle : O(1)
cost of removing from the back: O(1), , more expensive than vector's or deque's
cost of indexing : NOT provided
*/

/*
stack (LIFO)
push -> push_back
pop  -> pop_back
size
top  -> back
empty
*/

// inheritance (extend), this is what Java does

class stack : public vector {...}

// composition (contain), this is the preferred approach

template <typename T>
class stack {
    private:
        vector<T> _v;

int main () {
    stack<int> s;
    return 0;}

/*
use inheritance when you want BOTH of these:
    implementation
    interface
use composition when you ONLY want:
    implementation
*/

template <typename T, typename C = deque<T>>
class stack {
    private:
        C _c;

    ...};

int main () {
    stack<int> s;
    stack<int, vector<int>> s;
    stack<int, deque<int>>  s;
    stack<int, list<int>>   s;
    return 0;}
