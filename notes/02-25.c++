// -----------
// Mon, 25 Feb
// -----------

/*
FoCS
    please track and go
    really worth your time

Lab Sessions
    Wed, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF,  12-12:45pm,   GDC 6.302
        TTh, 3-4pm,        Zoom, https://us04web.zoom.us/j/412178924
    Brian
        M,   12-1pm,       GDC 3.302
    Katherine
        T,   9:30-10:30am, GDC 1.302

Piazza
    ask and answer questions
    please be proactive
*/

template <typename T>
class RangeIterator {
    public:
        RangeIterator (T x)
            ...

        T operator * () const
            ...

        ? operator == (RangeIterator rhs)
            ...

        ? operator != (RangeIterator rhs)
            return !(*this == rhs)

        RangeIterator& operator ++ ()
            ...

        RangeIterator operator ++ (int)
            ...

RangeIterator<int> b = 2;
cout << *b;
cout << b.operator*();
