// -----------
// Fri, 15 Feb
// -----------

/*
FoCS
    please track and go
    really worth your time

Lab Sessions
    Wed, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF,  12-12:45pm,   GDC 6.302
        TTh, 3-4pm,        Zoom, https://us04web.zoom.us/j/412178924
    Brian
        M,   12-1pm,       GDC 3.302
    Katherine
        T,   9:30-10:30am, GDC 1.302

Piazza
    ask and answer questions
    please be proactive
*/

bool equal (const int* b, const int* e, const int* x) {
    while (b != e) {
        if (*b != *x)
            return false;
        ++b;
        ++x;}
    return true;}

template <typename T, typename T>
bool equal (T* b, T* e, T* x) {
    while (b != e) {
        if (*b != *x)
            return false;
        ++b;
        ++x;}
    return true;}

// Test #1: T -> const int

template <typename T1, typename T2>
bool equal (T1* b, T1* e, T2* x) {
    while (b != e) {
        if (*b != *x)
            return false;
        ++b;
        ++x;}
    return true;}

// Test #2: T1 -> const int; T2 -> const long

/*
many containers in C++
    vector
    list
    deque
    set
    map
    ...
*/

// Java

ArrayList x = new ArrayList(...);
Iterator  p = x.iterator();
while (p.hasNext()) {
    ...
    ? v = p.next();
    ...}

// C++

list<int> x(...);
list<int>::iterator b = begin(x);
list<int>::iterator e = end(x);
while (b != e) {
    ? v = *b;
    ++b;}

template <typename I1, typename I2>
bool equal (I1 b, I1 e, I2 x) {
    while (b != e) {
        if (*b != *x)
            return false;
        ++b;
        ++x;}
    return true;}

// Test #3: I1 -> list<int>::const_iterator; I2 -> list<long>::const_iterator
