// -----------
// Fri,  8 Mar
// -----------

/*
FoCS
    please track and go
    really worth your time

Lab Sessions
    Wed, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF,  12-12:45pm,   GDC 6.302
        TTh, 3-4pm,        Zoom, https://us04web.zoom.us/j/412178924
    Brian
        M,   12-1pm,       GDC 3.302
    Katherine
        T,   9:30-10:30am, GDC 1.302

Piazza
    ask and answer questions
    please be proactive
*/

/*
stack (LIFO)
push -> push_back
pop  -> pop_back
size
top  -> back (2 of them)
empty
stack can have these as backing containers:
    vector
    deque (default)
    list
*/

/*
queue (FIFO)
push -> push_back
pop  -> pop_front
size
back  (2 of them)
front (2 of them)
empty
queue can have these as backing containers:
    deque (default)
    list
*/

/*
priority_queue
needs <
push -> push_back, push_heap
pop  -> pop_heap, pop_back
size
top  -> back (1 of them)
empty
priority_queue can have these as backing containers:
    vector (default)
    deque
*/

template <typename T>
class stack {
    ...
    public:
        const T& top () const {
            return ...}};
        T& top () {
            return ...}};

int main () {
    stack<int> x;
    x.push(2);
    cout << x.top(); // 2
    x.top() = 3;

    const stack<int> cx;
    cx.push(2);
    cout << cx.top();   // 2
    cx.top() = 3;       // no

    return 0;}

/*
set
    red-black binary search tree
        needs <
unordered_set
    hashtable
        elements need to be hashable
*/

/*
map
    red-black binary search tree
    needs < on the keys
unordered_map
    hashtable
        keys need to be hashable
*/

template <typename T, typenane C = deque<T>>
class stack {
    private:
        C _c;
    public:
        // stack () {}
        //stack () :     // clearer, but no better
            _c ()
            {}
        stack () = default; // best

        void push (const T& v) {
            _c.push_back(v);}

        void pop () {
            _c.pop_back();}

        int size () const {
            return _c.size();}

        bool empty () const {
            return _c.empty();}

        const T& top () const {
            return _c.back();}

        T& top () {
            return _c.back();}}
